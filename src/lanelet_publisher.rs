use rayon::prelude::*;
use geometry_msgs::msg::Point;
use lanelet_msgs::msg::{Lanelet,LineString};
use std::{sync::Arc, time::Duration};
use std_msgs::msg::Header;
use builtin_interfaces::msg::Time;
struct DummyPub {
    node: Arc<rclrs::Node>,
    _publisher: Arc<rclrs::Publisher<Lanelet>>,
}

impl DummyPub {
    fn new(context: &rclrs::Context) -> Result<Self, rclrs::RclrsError> {
        let node = rclrs::create_node(context, "target_pose").unwrap();
        let _publisher = node
            .create_publisher("target_pose", rclrs::QOS_PROFILE_DEFAULT)
            .unwrap();
        Ok(Self { node, _publisher })
    }

    /// Publishes a "Hello World" message on the publisher.
    ///
    /// Creates a StringMsg with "Hello World" as the data, publishes it on
    /// the `_publisher`, and returns a Result. This allows regularly publishing
    /// a simple message on a loop.
    fn publish_data(&self) -> Result<(), rclrs::RclrsError> {
        let msg = Lanelet {
            meta_info: Header {
                frame_id: "map".to_string(),
                stamp: Time {
                    sec: (self.node.get_clock().now().nsec /10_i64.pow(9)) as i32,
                    nanosec: (self.node.get_clock().now().nsec %10_i64.pow(9)) as u32,
                },
            },
            left_boundary: LineString {
                line_string: (0..10)
                    .into_par_iter()
                    .map(|i| i as f64)
                    .map(|i| Point {
                        x: (i * 10.0),
                        y: -20.0,
                        z: 0.0,
                    })
                    .collect::<Vec<Point>>(),
            },
            right_boundary: LineString {
                line_string: (0..10)
                    .into_par_iter()
                    .map(|i| i as f64)
                    .map(|i| Point {
                        x: (i * 10.0),
                        y: 20.0,
                        z: 0.0,
                    })
                    .collect::<Vec<Point>>(),
            },
        };

        self._publisher.publish(msg).unwrap();
        Ok(())
    }
}

/// The main function initializes a ROS 2 context, node and publisher,
/// spawns a thread to publish messages repeatedly, and spins the node
/// to receive callbacks.
///
/// It creates a context, initializes a SimplePublisherNode which creates
/// a node and publisher, clones the publisher to pass to the thread,  
/// spawns a thread to publish "Hello World" messages repeatedly, and
/// calls spin() on the node to receive callbacks. This allows publishing
/// messages asynchronously while spinning the node.
fn main() -> Result<(), rclrs::RclrsError> {
    let context = rclrs::Context::new(std::env::args()).unwrap();
    let publisher = Arc::new(DummyPub::new(&context).unwrap());
    let publisher_other_thread = Arc::clone(&publisher);
    std::thread::spawn(move || std::iter::repeat(()).for_each(|()|{
        std::thread::sleep(Duration::from_millis(1));
        publisher_other_thread.publish_data().unwrap();
    }));
    rclrs::spin(publisher.node.clone())
}
