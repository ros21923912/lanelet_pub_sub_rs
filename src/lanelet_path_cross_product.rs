use std::{sync::{Arc,Mutex},env};
use anyhow::Result;
use rayon::prelude::*;
use nalgebra::Vector3;
use lanelet_msgs::msg::{Lanelet};
use geometry_msgs::msg::{Point,Pose,PoseStamped,Quaternion};
use std_msgs::msg::Header;
use nav_msgs::msg::Path;
use builtin_interfaces::msg::Time;


fn quaternions_from_points(point1: Point, point2: Point) -> Quaternion {

    let point1_vec = Vector3::new(point1.x, point1.y, point1.z);
    let point2_vec = Vector3::new(point2.x, point2.y, point2.z);
  
    let vector_ab =point1_vec-point2_vec;
    let vector_ab_normalized = vector_ab / vector_ab.norm();
    
    let theta = vector_ab_normalized.x.acos();
                
    let half_theta = theta/2.0;
    
    Quaternion{
        w:half_theta.cos(),
        x:vector_ab_normalized[0].sin() * half_theta,
        y:vector_ab_normalized[1].sin() * half_theta,
        z:vector_ab_normalized[2].sin() * half_theta
    }
  }

fn middle_point(point1:Point,point2:Point) -> Point{
    Point{
        x:(point1.x+point2.x)/2.0,
        y:(point1.y+point2.y)/2.0,
        z:(point1.z+point2.z)/2.0
    }
}

struct TempValues{
    left_point:Vec<Point>,
    right_point:Vec<Point>,
    middle_point:Point,
    index:usize
}

struct PathPlanning {
    node: Arc<rclrs::Node>,
    _publisher: Arc<rclrs::Publisher<Path>>,
    _subscription:Arc<rclrs::Subscription<Lanelet>>,
    data: Arc<Mutex<Option<Lanelet>>>
}
impl PathPlanning{
    fn new(context: &rclrs::Context) -> Result<Self, rclrs::RclrsError>{
        let node = rclrs::create_node(context, "lanelet_path_conversion").unwrap();
        let data = Arc::new(Mutex::new(None));
        let data_cb = Arc::clone(&data);
        let _publisher = node
            .create_publisher("citicar_roadway_path", rclrs::QOS_PROFILE_DEFAULT)
            .unwrap();
        let _subscription = node.create_subscription(
            "target_pose",
            rclrs::QOS_PROFILE_DEFAULT,
            move |msg:Lanelet|{*data_cb.lock().unwrap() = Some(msg);},
            ).unwrap();
        Ok(Self { node, _publisher,_subscription,data })
    }

    fn generate_path(&self) -> Result<(), rclrs::RclrsError> {
        if let Some(lanelet) = &*self.data.lock().unwrap(){
            let array_length=lanelet.left_boundary.line_string.len();
            let path = Path{
                header: Header{
                    frame_id: "map".to_string(),
                    stamp: Time {
                        sec: (self.node.get_clock().now().nsec /10_i64.pow(9)) as i32,
                        nanosec: (self.node.get_clock().now().nsec %10_i64.pow(9)) as u32,
                    },
                },
                poses:(0..=array_length-1)
                .into_par_iter().
                map(|index|{TempValues{
                    left_point:lanelet.left_boundary.line_string.clone(),
                    right_point:lanelet.right_boundary.line_string.clone(),
                    middle_point:middle_point(lanelet.left_boundary.line_string[index].clone(),lanelet.right_boundary.line_string[index].clone()),
                    index
                }})
                .map(|temp_values|{
                    PoseStamped{
                        header:lanelet.meta_info.clone(),
                        pose:Pose{
                            position: temp_values.middle_point.clone(),
                            orientation:quaternions_from_points(
                                match temp_values.index+1 < array_length {
                                    true => middle_point(temp_values.left_point[temp_values.index+1].clone(),temp_values.right_point[temp_values.index+1].clone()),
                                    _ => middle_point(
                                        Point{
                                            x:temp_values.left_point[temp_values.index].x.clone()*2.0-temp_values.left_point[temp_values.index-1].x.clone(),
                                            y:temp_values.left_point[temp_values.index].y.clone()*2.0-temp_values.left_point[temp_values.index-1].y.clone(),
                                            z:temp_values.left_point[temp_values.index].z.clone()*2.0-temp_values.left_point[temp_values.index-1].z.clone(),
                                        },
                                        Point{
                                            x:temp_values.right_point[temp_values.index].x.clone()*2.0-temp_values.right_point[temp_values.index-1].x.clone(),
                                            y:temp_values.right_point[temp_values.index].y.clone()*2.0-temp_values.right_point[temp_values.index-1].y.clone(),
                                            z:temp_values.right_point[temp_values.index].z.clone()*2.0-temp_values.right_point[temp_values.index-1].z.clone(),
                                        }
                                    )
                                },
                                temp_values.middle_point.clone()
                            )
                        }
                    }
                }).collect::<Vec<PoseStamped>>(),
            };
        self._publisher.publish(path).unwrap();}
        Ok(())    
    }
}

fn main() -> Result<(), rclrs::RclrsError> {
    let context = rclrs::Context::new(env::args()).unwrap();
    let path_planning = Arc::new(PathPlanning::new(&context).unwrap());
    let publisher_other_thread = Arc::clone(&path_planning);
    std::thread::spawn(move || {
        std::iter::repeat(())
        .for_each(|()| publisher_other_thread.generate_path().unwrap())
    });
    let node_ref = &path_planning.node;
    rclrs::spin(node_ref.clone())
}
